#!/usr/bin/env python
#-*-coding:utf8;-*-##########################
#qpy:2#######################################
#qpy:kivy####################################
#####                                   #####
#####        _       _                  #####
#####       | |_   _(_) ___ ___         #####
#####    _  | | | | | |/ __/ _ \        #####
#####   | |_| | |_| | | (_|  __/        #####
#####    \___/ \__,_|_|\___\___|        #####
#####                                   #####
#####  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  #####
##### |         Austin Byron          | #####
#####  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  #####
#####             ____                  #####
#####            | __ )  __ _ _ __      #####
#####            |  _ \ / _` | '__|     #####
#####            | |_) | (_| | |        #####
#####            |____/ \__,_|_|        #####
#####                                   #####
#####                                   #####
#############################################
#############################################
#############################################


import os
import sys
import json
import time
from collections import OrderedDict as OD
from pprint import pprint as pP
import re
from datetime import date, datetime, timedelta
from email_validator import validate_email, EmailNotValidError
from mysql import connector
# import kivy
# from kivy.config import Config 
# Config.set('graphics', 'width', '1024') 
# Config.set('graphics', 'height', '768')
from kivy.core.window import Window
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
#from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.carousel import Carousel
from kivy.uix.scrollview import ScrollView
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from kivy.uix.popup import Popup
from kivy.uix.behaviors import ButtonBehavior
from kivy.graphics import *
from kivy.graphics import (Rectangle, RoundedRectangle, Ellipse, Color)
from kivy.animation import Animation
from kivy.clock import Clock
# from kivy.lang.builder import Builder
from kivy.factory import Factory
# from kivy.properties import (BooleanProperty, ObjectProperty)
# from kivy.properties import ObjectProperty
from hover import HoverBehavior
from functools import partial
from weezeBuddy import theJuice

from PIL import Image as pImage
from bs4 import BeautifulSoup as BS
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from random import randint

Factory.register('HoverBehavior', HoverBehavior)

cAttrs = OD([('id', ''), ('juice_id', ''), ('first_name', ''), ('last_name', ''), ('email', ''), ('punches', ''), ('date_started', ''), ('date_expires', '')])

customerButtons = ('Use Punch', 'Add Punches', 'Date')
print cAttrs

basedir = os.path.abspath(os.path.dirname(__file__))
imgDir = os.path.join(basedir, 'img')


class startPage(Carousel):
    def __init__(self, **args):
        super(startPage, self).__init__(**args)
        self.spices = False
        self.size_hint = (None, None)
        self.spSize()
        self.bind(size=self.winSize)

        self.userAdd = mkUserAdd()
        self.userAdd.search_btn.bind(on_press=self.addUser)
        self.add_widget(self.userAdd)

        self.userSearch = mkUserSearchBuilder()
        self.userSearch.search_btn.bind(on_press=self.juicerSearch)
        self.add_widget(self.userSearch)

        print 'current_slide: {}'.format(self.current_slide)
        print 'startPage.size: {}'.format(self.size)
        self.index = 1
        print 'slides: {}'.format(self.index)

        # populate the database with junk data for testing!
        # spiceJuice()

    def winSize(self, obj, siz):
        self.spSize()
        self.userSearch.size = self.size
        self.userAdd.size = self.size
        if self.spices != False:
            # self.spices.size = (siz[0] - 20, siz[1])
            self.spices.size = siz
            self.spices.mkSpicSP()

    def spSize(self):
        self.size = (Window.size[0], Window.size[1] - Window.size[1] * .07) 
        self.pos_hint = {'center_x': 0.5, 'top': 1}

    def addUser(self, tgt):
        self.userAdd.search_btn.unbind(on_press=self.addUser)
        print 'hi you did it!: {} --> (fName_txt.text: {}, lName_txt.text: {}, userID_txt.text: {})'.format(tgt.id, self.userAdd.fNameBX.juicy_txt.text, self.userAdd.lNameBX.juicy_txt.text, self.userAdd.juiceIdBX.juicy_txt.text)

        args1 = {'first_name': self.userAdd.fNameBX.juicy_txt.text.capitalize(),
                 'last_name': self.userAdd.lNameBX.juicy_txt.text.capitalize(),
                 'juice_id': self.userAdd.juiceIdBX.juicy_txt.text,
                 'email': self.userAdd.emailBX.juicy_txt.text,
                 'punches': self.userAdd.noOfPunches}

        juiceIt = theJuice()

        juicers = juiceIt.mixItDN(**dict(args1.items()[:2]))
        print 'this fucking thing man ==> {}'.format(juicers)
        if juicers[0] is False:
            # There is no data in the database for the user or regected by regx
            result = juiceIt.mixItUP(**args1)
            if result[0] is False:
                # is regected by regx
                print '\nFUCK_1\n'
                print result

                if result[1][0]:
                    # here we handle the mysql connection errors.
                    # User permissionson juicebar local or external database
                    # has not been granted
                    retMsg = '{}'.format('\n'.join(result[1][1]))
                    print retMsg
                else:
                    retMsg = '{}\n\nWould you like to delete this fields text?'.format(result[1][1])
                    print retMsg

                popit = juicePop(auto_dismiss=False, title='Error: Database not found!', msg=retMsg, ask=True)
                # popit.bind(on_dismiss=self.reply)
                popit.open()
            else:
                print 'success_1: {}     <<=========LO00KING'.format(result)
                retMsg = 'First Name: {}\nLast Name: {}\nEmail: {}\nJuicd ID: {}'.format(result[1]['first_name'], result[1]['last_name'], result[1]['email'], result[1]['juice_id']) # pylint: disable=E1126
                popit = juicePop(auto_dismiss=True, title='User Data Entered Succuessfully.', msg=retMsg, ask=True)
                popit.bind(on_dismiss=self.clearAllReToHome)
                popit.open()
        else:
            # There are other users that have data that could cause a conflict. Sort that shit out man!
            juicers = juicers[1]
            del juicers['query']

            isUinq = True

            for n, val in juicers.items():
                cnt = 0

                for k, v in args1.items()[:2]:
                    if val[k] == v:
                        cnt += 1

                if cnt == 3 and isUinq is True:
                    isUinq = False

            if isUinq:
                result = juiceIt.mixItUP(**args1)
                print result
                if result[0] is False:
                    print '\nFUCK_2\n'
                    # I think that this where they would fail the sql checks network-manager-openvpn network-manager-openvpn-gnome network-manager-pptp network-manager-vpnc
                    #  we are working this out above

                    if result[1][0]:
                        retMsg = 'This should Never Happen {}'.format(result[1][1])
                        print retMsg
                    else:
                        retMsg = '{}\n\nWould you like to delete this fields text?'.format(result[1][1])
                        print retMsg

                    # needs further testing. I should have made proper notes when writing this. I see now the folly of my ways!
                    popit = juicePop(auto_dismiss=False, title='User Data Rejected', msg=retMsg, ask=True)
                    # popit.bind(on_dismiss=self.reply)
                    popit.open()

                else:
                    ## there might be an error here
                    print 'success_2: {}'.format(result)
                    retMsg = 'First Name: {}\nLast Name: {}\nEmail: {}\nJuicd ID: {}'.format(result[1]['first_name'], result[1]['last_name'], result[1]['email'], result[1]['juice_id']) # pylint: disable=E1126
                    popit = juicePop(auto_dismiss=True, title='User Data Entered Succuessfully.', msg=retMsg, ask=True)
                    popit.bind(on_dismiss=self.clearAllReToHome)
                    popit.open()
            else:
                # print 'User already exists!'
                popit = juicePop(auto_dismiss=False, title='User Data', msg='The user already exists. \nTry another Juice ID.', ask=True)
                # popit.bind(on_dismiss=self.reply)
                popit.open()
        self.userAdd.search_btn.bind(on_press=self.addUser)

    def clearAllReToHome(self, obj):
        # remove spices
        if self.spices is not False:
            self.remove_widget(self.spices)
            self.remove_widget(self.carSel)
            self.spices = False

        ## Do loop here and unbind all spice object using if 'spices_' in obj
        ##
        ##

        # remove text from userAdd textfeilds
        self.userAdd.clearAllTxt()

        # remove text from useSearch textfeilds
        self.userSearch.clearAllTxt()

        # return to usersearch? Maybe add punches? For now just return to search!
        self.load_slide(self.userSearch)


    def juicerSearch(self, tgt):
        self.userSearch.search_btn.unbind(on_press=self.juicerSearch)
        print self.slides

        print 'hi you did it!: {} --> (fName_txt.text: {}, lName_txt.text: {}, userID_txt.text: {})'.format(tgt.id, self.userSearch.fNameBX.juicy_txt.text, self.userSearch.lNameBX.juicy_txt.text, self.userSearch.juiceIdBX.juicy_txt.text)
        juiceIt = theJuice()

        args1 = {'first_name': self.userSearch.fNameBX.juicy_txt.text.capitalize(),
                 'last_name': self.userSearch.lNameBX.juicy_txt.text.capitalize(),
                 'juice_id': self.userSearch.juiceIdBX.juicy_txt.text}

        for key, arg in args1.items():
            if arg == '' or arg is None:
                del args1[key]

        juicers = juiceIt.mixItDN(**args1)

        if juicers[0] is False:
            if self.spices is not False:
                self.remove_widget(self.carSel)
                self.remove_widget(self.spices)
                self.spices = False

            self.userAdd.fNameBX.juicy_txt.text = self.userSearch.fNameBX.juicy_txt.text
            self.userAdd.lNameBX.juicy_txt.text = self.userSearch.lNameBX.juicy_txt.text
            self.userAdd.juiceIdBX.juicy_txt.text = self.userSearch.juiceIdBX.juicy_txt.text

            self.load_slide(self.userAdd)
            self.userSearch.search_btn.bind(on_press=self.juicerSearch)
            return False
        else:

            if self.spices is not False:
                self.remove_widget(self.spices)
                self.remove_widget(self.carSel)

            ## we are working here
            # self.spiceHold
            
            self.carSel = Carousel(size_hint=(None, None), size=Window.size, direction='bottom')

            self.spices = spiceRack(size=Window.size, juicers=juicers[1], carousel=self.carSel)

            self.carSel.add_widget(self.spices)
            self.add_widget(self.carSel)
            self.load_slide(self.carSel)
            self.userSearch.search_btn.bind(on_press=self.juicerSearch)


class powerNavBar(FloatLayout):
    def __init__(self, *args, **kargs):
        super(powerNavBar, self).__init__(**kargs)
        self.orientation = 'horizontal'
        self.size_hint = (None, None)
        self.powSize()

        self.bind(size=self.winSize, pos=self.winSize)


    def powSize(self):
        self.height = Window.size[1] * .07
        self.width = Window.size[0]
        self.pos_hint = {'center_x': 0.5, 'bottom': 1}

        self.canvas.before.clear()
        with self.canvas.before:
            Color(0.0, 0.0, 0.0, 0.35)
            RoundedRectangle(size=self.size, pos=self.pos, radius=[0, 10, 10, 0])


    def winSize(self, obj, atr):
        self.powSize()


class spiceRack(FloatLayout):
    def __init__(self, **args):
        super(spiceRack, self).__init__(**args)
        self.card = False

        self.id = 'spiceRack'
        self.size_hint = (None, None)

        self.carSel = args['carousel']

        self.bind(size=self.winSize)
        self.bind(pos=self.winPos)

        queryCont = BoxLayout(size=(self.size[0] * .9, self.size[1] * .1))
        query = Label(text=args['juicers']['query'], font_size=self.size[1] * .05, pos_hint={'center_x': 0.5, 'center_y': 0.95})

        del args['juicers']['query']

        queryCont.add_widget(query)
        self.add_widget(queryCont)

        self.rack = ScrollView(size_hint=(None, None), pos_hint={'center_x': 0.5, 'bottom': 0}, size=(self.width * .95, self.height * .87))
        self.boxer = FloatLayout(id='boxer', size_hint=(None, None), size=(self.rack.width, 10))

        self.spices = []
        self.juicers = args['juicers']

        print len(self.juicers)

        for key, val in enumerate(self.juicers.items()):
            tID = 'spice_{}'.format(key + 1)
            spice = spiceGrid(id=tID, size_hint=(None, None),
                              juice_id=val[1]['juice_id'],
                              first_name=val[1]['first_name'],
                              last_name=val[1]['last_name'])

            self.spices.append(spice)
            spice.bind(on_press=self.spiceClick)
            self.mkSpicSP()
            self.boxer.height = self.boxer.height + self.height * 0.09 + 10
            self.boxer.add_widget(spice)

        self.rack.add_widget(self.boxer)
        self.add_widget(self.rack)

    def mkSpicSP(self):

        self.canvas.before.clear()
        with self.canvas.before:
            Color(0.0, 0.0, 0.0, 0.35)
            Rectangle(size=self.size, pos=self.pos)

        self.rack.size = (self.width * .95, self.height * .87)
        self.boxer.size = (self.rack.width, 10)
        for key, spice in enumerate(self.spices):
            spice.size = (self.boxer.width * 0.98, self.height * 0.09)
            spice.pos = (((self.boxer.width - self.boxer.width * 0.98) * .5), 10 if key == 0 else self.boxer.height)
            self.boxer.height = self.boxer.height + self.height * 0.09 + 10
            spice.mkPos()

        if not self.card is False:
            self.carSel.size = self.size
            self.carSel.pos = self.pos
            self.card.size = self.size
            self.card.pos = self.pos


    def winSize(self, obj, siz):
        self.size = siz
        self.mkSpicSP()


    def winPos(self, obj, pos):
        self.pos = pos
        self.mkSpicSP()


    def spiceClick(self, *sarg):
        print 'sarg.id: {}'.format(sarg[0].id.strip('spice_'))
        print 'biuld your punchcard man!'
        tID = sarg[0].id.strip('spice_')

        juicerInfo = False

        juicerInfo = self.juicers[tID]

        self.card = juicyCard(auto_dismiss=True, juicer=juicerInfo, title='Make a punch:')

        self.carSel.add_widget(self.card, index=1)
        self.carSel.load_slide(self.card)


##############################################
############ These are the popups ############
##############################################

### Define the main popup: look, functionality.
#  This is the base for all Puopups.

class juicyPopUp(Popup):
    ''' Define the main popup: look, functionality.
            This is the base for all Puopups. '''
    def __init__(self, *args, **kwargs):
        super(juicyPopUp, self).__init__(*args, **kwargs)

        ###### popup settings ######
        self.size_hint = (None, None)

        ### Default size ratio for the popup.
        ## 90 % of window width, 60% of window height.
        self.rSize = (0.9, 0.8)

        ### Set size and position.
        ## Position in the center of the window.
        self.size = (Window.size[0] * self.rSize[0], Window.size[1] * self.rSize[1])
        self.pos = ((Window.size[0] - self.width) * .5, (Window.size[1] - self.width) * .5)
        
        ### Set the separator
        self.separator_color = (0, 0, 0, 1.0)
        self.separator_height = 10

        ### Set title: text, font, color, alignment
        # self.title = 'Remove Punches:'
        self.title_align = 'left'
        self.title_color = (0, 0, 0, 1.0)
        # self.title_font = Color(0.5, 0.5, 0.5, 1.0)
        self.title_size = 25
        
        ### Settings for the background
        ## Set Background image to a blank 1x1px image
        ## so we can overide the background
        self.background = os.path.join(imgDir, 'blank.png')

        ## Do auto dismiss logic:
        if self.auto_dismiss is False:
            ## Auto dissmoss is false. 
            ## Hide the background. 
            ## This makes it imposible to exit without
            ## a button. 
            self.background_color = (0, 0, 0, 0)
        else:
            ## Auto dissmiss  is true.
            ## Set the background color.
            ## user can click the background to dismiss
            ## the popup
            self.background_color = (0, 0, 0, .2)


        ### setup the new background image
        ## Set the OS independant image location
        self.card_path = os.path.join(os.getcwd(), 'img', 'juicyCard.png')

        ### Create a layout for the background to be placed in
        self.box = FloatLayout(size_hint=(None, None), size=self.size, pos=self.pos)

        ### Make sure the background file exists.
        ## if so create the image and get texture size
        if os.path.exists(self.card_path):
            # src = "http://placehold.it/480x270.png&text=slide-%d&.png" % i
            # image = AsyncImage(source=src, allow_stretch=True)

            self.cardImg = Image(source=self.card_path)
            self.cardImgSize = self.cardImg.texture_size
        else:
            print 'this is not the path you seek! : {}'.format(self.card_path)

        #######################

        ### Set the native popups to The layout we created above
        ## the popup.content is the only widget allowed so add content
        ## to self.box to update the popup.content
        self.content = self.box

        ### Set size and position of the box
        self.cardSize()
        ### Bind the size/position to the size/postion of the window
        self.bind(pos=self.winSize, size=self.winSize)


    def winSize(self, obj, siz):
        '''This function defines the size and position of everything added
        to the box.'''
        self.cardSize()

    def cardSize(self):
        ''' Make size and position for the background image.'''

        self.size = (Window.size[0] * self.rSize[0], Window.size[1] * self.rSize[1])
        self.pos = ((Window.size[0] - self.width) * .5, (Window.size[1] - self.height) * .5)

        self.box.size = (self.width * .9, self.height * .9) 
        self.box.pos = ((Window.size[0] - (self.width  * .9)) * .5, (Window.size[1] - (self.height * .9)) * .5)

        ### Make the canvas object. The New background image will be used.
        self.canvas.before.clear()
        with self.canvas.before:
            Color(1, 1, 1, 1.0)
            RoundedRectangle(texture=self.cardImg.texture, size=self.size, pos=self.pos, radius=[10, 10, 10, 10])


class selectPunchsToAdd(juicyPopUp):
    ''' The main functionality and looks are borrowed from the 
        class juicyPopUp(). 

        This class is for the add punches to card function. '''
    def __init__(self, *args, **kwargs):
        super(selectPunchsToAdd, self).__init__(*args, **kwargs)

        ### Set auto dismiss  to true as default behaviour.
        self.auto_dismiss = True

        ### Set default size ratio to
        ## 35% of width, height
        self.rSize = (.45, .50)

        ### Set default punches
        self.punches = 5

        print self.width

        ### punch_btn is the text that the main button displays
        ## It is passed to this class as kwargs
        self.punch_btn = kwargs['punchTxt']

        ### Number of buttons to make
        self.nOfButtons = 4

        ### create a new layout for the buttons. 
        self.punchesGrid = GridLayout()

        ### add new layout to popup.content
        self.content = self.punchesGrid

        ### Set rows and columns
        self.punchesGrid.cols = 2
        self.punchesGrid.rows = 2

        ### Set size hint to none, 
        ## we will set the size in the winSize() function
        self.punchesGrid.size_hint = (None, None)

        ### Set other gridlayout properties.
        self.punchesGrid.padding = [5, 5, 5, 5]
        self.punchesGrid.row_force_default = False

        ### Create all the buttons we need.
        ## punches to add #
        for numBtn in range(1, 5):

            ## Build Add Punches Button
            self.numBtn_btn = juiceBtn()

            ## Add button to gridlayout
            self.punchesGrid.add_widget(self.numBtn_btn)

            ## Setup the id. We will use it to resize the buttons in a loop
            ## in the mkPunches() function
            self.numBtn_btn.id = '{}_btn'.format(numBtn * 5)

            ## Set the button text
            self.numBtn_btn.text = '{}'.format(numBtn * 5)

            ## set size hint to none. We will set the size in the 
            ## mkPunches() function.
            self.numBtn_btn.size_hint = (None, None)

            ## Bind the button to the addPunches() function
            self.numBtn_btn.bind(on_press=self.addPunches)

        ### Set size and position of the punchesGrid and the background
        self.cardSize()
        self.mkPunches()

        ### Unbind the winSize() function from the juicyPopUp() class
        ## so we can modify it a bit. Replace the binding to new
        ## winSize2() function

        self.unbind(pos=self.winSize, size=self.winSize)
        self.bind(pos=self.winSize2, size=self.winSize2)

    def addPunches(self, tgt):
        ''' This function sets the punches attribute! '''
        ### Get the numerical value of the button selected
        self.punch_btn.text = '{} punches to be added.'.format(tgt.id.split('_')[0])

        ### Keep this for now. We may use something like this for the spice rack!
        # unbind all the buttons!
        # for btn in self.punchesGrid.walk():
        #     print btn.id
        #     btn.unbind(on_press=self.addPunches)

        ### Set punches as int and dismiss
        ## To get value use self.punches where self
        ## is the name of the instance of this class. 
        self.punches = int(tgt.id.split('_')[0])
        self.dismiss()

    def winSize2(self, obj, val):
        ''' This function handles the resize events. '''
        ### Set the background size/pos
        self.cardSize()
        ### Set the punchesGrid elements
        self.mkPunches()

    def mkPunches(self):
        ''' This function sets the size and position 
            of the elements created in this class '''
        ### Set the size and position of the gridlayout
        self.punchesGrid.size = ((self.width * self.rSize[0]), (self.height * self.rSize[1]))
        self.punchesGrid.pos_hint = {'center_x': .65, 'center_y': .65}

        ### Loop through the gridlayout and resize
        ## all the buttons
        for child in self.punchesGrid.walk():
            ## since we know the chile has a name 
            ## we can get that child suing the id we set.
            if not child.id is None and '_btn' in child.id:
                ## Pardon the crazy math. It does not make any sence
                ## but it works.
                child.size = ((self.punchesGrid.width * .25) * 4, (self.punchesGrid.height * .25) * 2.80)
                child.pos_hint = {'center_x': .55, 'center_y': .5}


class juicyCard(juicyPopUp):
    ''' This class of popup is for the actual punch card itself. '''
    def __init__(self, *args, **kwargs):
        super(juicyCard, self).__init__(*args, **kwargs)

        self.juicer = kwargs['juicer']
        # self.auto_dismiss = True
        self.background_color = (0, 0, 0, .2)

        pP(self.juicer.items())

        self.mainCont = FloatLayout(size_hint=(None, None))

        ########################
        ### ** About User ** ###
        ########################

        self.uInfo = FloatLayout(size_hint=(None, None))

        self.uInfo.rows = 3
        self.uInfo.cols = 2

        # --- make First Name ID Labels and container
        fName = spiceLBL(id='fName_lbl', first_name=self.juicer['first_name'])
        self.uInfo.add_widget(fName)

        # --- make Last Name ID Labels and container
        lName = spiceLBL(id='lName_lbl', last_name=self.juicer['last_name'])
        self.uInfo.add_widget(lName)

        # --- make Juice ID Labels and container
        juiceID = spiceLBL(id='jBox_lbl', juice_id=self.juicer['juice_id'])
        self.uInfo.add_widget(juiceID)

        # --- make Date Started ID Labels and container
        dStarted = spiceLBL(id='dStarted_lbl', date_started=self.juicer['date_started'])
        self.uInfo.add_widget(dStarted)

        # --- make Date Expires ID Labels and container
        dExpires = spiceLBL(id='dExpires_lbl', date_expires=self.juicer['date_expires'])
        self.uInfo.add_widget(dExpires)

        ### Create a map of three rows: [row, row, row]. and each row
        ## has two columns [[col[0], col[1]], [col[0], col[1], [col[0], col[1]]
        ## this is used for positioning in mkSize()
        self.dMap = [[fName, lName], [dStarted, dExpires], [juiceID, False]]


        #######################
        ### **  Punches  ** ###
        #######################
        
        self.funkyPunch = mkYang(id='pPunch_0', size_hint=(None, None), mode='yang', isBtn=True)
        self.mainCont.add_widget(self.funkyPunch)

        self.pPunches = punch2Punch(size_hint=(None, None), user_punches=int(self.juicer['punches']), punch=self.funkyPunch)
        self.mainCont.add_widget(self.pPunches)

        self.mainCont.add_widget(self.uInfo)
        self.content = self.mainCont

         ## position the elements with mkSize() function
        self.cardSize()
        self.mkSize()

        ### Unbind and rebind the winSize() function
        ## to winSize2()
        self.unbind(pos=self.winSize, size=self.winSize)
        self.bind(pos=self.winSize2, size=self.winSize2)

    def winSize2(self, obj, val):
        self.cardSize()
        self.mkSize()

    def mkSize(self):

        self.mainCont.size = self.size
        self.mainCont.pos = self.pos

        self.uInfo.size = ((self.width - 40), (self.height  * .38))
        self.uInfo.pos = (self.mainCont.x + 20, self.mainCont.y + 20)

        dTop = self.uInfo.y + self.uInfo.height

        self.uInfo.col_width = self.uInfo.width / self.uInfo.cols
        self.uInfo.row_height = self.uInfo.height / self.uInfo.rows

        for row in xrange(0, self.uInfo.rows):
            for col in xrange(0, self.uInfo.cols):
                if not self.dMap[row][col] is False:
                    # print 'Row: {}, Col: {}'.format(row, col)
                    self.dMap[row][col].size_hint = (.43, .35)
                    self.dMap[row][col].x = self.uInfo.x + self.uInfo.col_width * col
                    self.dMap[row][col].y = dTop - (self.uInfo.row_height * (row + 1) - (10 * row))


        self.uInfo.canvas.before.clear()
        with self.uInfo.canvas.before:
            Color(0, 0, 0, 0.15)
            RoundedRectangle(size=self.uInfo.size, pos=self.uInfo.pos, radius=[10, 10, 10, 10])

        ######

        self.pPunches.size = (self.width * .75, self.height  * .25)
        self.pPunches.pos = ((self.uInfo.x + (self.uInfo.width - self.pPunches.width)) * .5, dTop + (self.height * .05))


class punch2Punch(Widget):
    ''' This class creates the actual punch card punches to be punched
        by the user '''
    def __init__(self, *args, **kwargs):
        super(punch2Punch, self).__init__(*args, **kwargs)
        if not kwargs.has_key('user_punches'):
            self.userPunches = 0
        else:
            self.userPunches = kwargs['user_punches']

        self.punchLay = FloatLayout(size_hint=(None, None))
        self.btnLay = FloatLayout(size_hint=(None, None))
        self.add_widget(self.btnLay)
        self.add_widget(self.punchLay)

        for x in xrange(0, self.userPunches):
            if x == 0:
                ## Next drink is free!
                print 'next drink is free' 
                
            elif x >= 1:
                pPunch = mkYang(id='pPunch_{}'.format(x), size_hint=(None, None), mode='yang')
                self.punchLay.add_widget(pPunch)
            else:
                break
        
        self.punch = kwargs['punch']

        self.mkPunchSize()

        self.bind(size=self.punchSize, pos=self.punchSize)

    def punchSize(self, obj, val):
        self.mkPunchSize()


    def mkPunchSize(self, maxPunch=10):

        self.punchLay.size = (self.width * .75, self.height)
        self.punchLay.pos = self.pos

        self.btnLay.size = (self.width * .25, self.height)
        self.btnLay.pos = (self.x + self.punchLay.width, self.y)

        if self.userPunches >= maxPunch:
            rowHeight = (self.punchLay.height / (self.userPunches / maxPunch)) * .85
            rowWidth = self.punchLay.width / maxPunch
        else:
            print 'here'
            rowHeight = self.punchLay.width / self.userPunches - 1
            rowWidth = self.punchLay.width / self.userPunches - 1

        if self.userPunches >= maxPunch:
            offSet = self.punchLay.width - (rowWidth * maxPunch)
        else:
            offSet = self.punchLay.width - (rowWidth * self.userPunches)

        j = 0
        for i, child in enumerate(self.punchLay.children):

            if child.width * maxPunch < self.punchLay.width:
                child.height = rowHeight
                child.width = rowHeight
            else:
                child.height = rowWidth
                child.width = rowWidth
            
            if i <= 9:
                child.x = self.punchLay.x + offSet + (child.height * i)
                child.y = self.punchLay.y
            else:
                if i == j + 10:
                    j += 10
                child.x = self.punchLay.x + offSet + (child.height * int(str(i)[1]))
                child.y = self.punchLay.y + (child.height * int(str(j)[0]))

        # for child in self.btnLay.children:
        if self.btnLay.width > self.btnLay.height:
            self.punch.height = self.btnLay.height
            self.punch.width = self.btnLay.height
        else:
            self.punch.width = self.btnLay.width
            self.punch.height = self.btnLay.width
        self.punch.x = self.btnLay.x + (self.btnLay.width - self.punch.width) * .5
        self.punch.y = self.btnLay.y + (self.btnLay.height - self.punch.width) * .5

        self.canvas.after.clear()
        with self.canvas.after:
            Color(0, 0, 0, 0.15)
            RoundedRectangle(size=(self.width + 50, self.height + 20), pos=(self.x, self.y - 5), radius=[10, 10, 10, 10])


        self.canvas.before.clear()
        with self.canvas.before:
            StencilPush()

            #set mask
            RoundedRectangle(size=(self.width * .25, self.height), pos=(self.x + (self.width - self.width * .25), self.y), radius=[10, 10, 10, 10])

            #use mask
            StencilUse()

            Color(0, 0, 0, 0.)
            shade = RoundedRectangle()
            shade.size = self.size
            shade.pos = self.pos
            shade.radius = [10, 10, 10, 10]

            StencilPop()


class mkYang(ButtonBehavior, Widget, HoverBehavior):
    ''' This class creates the yin/yang image/button/animation '''
    def __init__(self, *args, **kwargs):
        super(mkYang, self).__init__(*args, **kwargs)
        self.baseDir = imgDir
        self.playing = False

        if kwargs.has_key('isBtn') and kwargs['isBtn'] is True:
            self.isBtn = True
        else:
            self.isBtn = False

        if kwargs.has_key('mode') and kwargs['mode'] == 'yang':
            self.iDir = os.path.join(self.baseDir, 'yang')
            self.isYang = True
        elif kwargs.has_key('mode') and kwargs['mode'] == 'yin':
            self.iDir = os.path.join(self.baseDir, 'yin')
            self.isYang = False

        self.imgDir = sorted(os.listdir(self.iDir), key=sortDirs)

        self.mkImg(rand=True)

        self.bind(size=self.imgSize, pos=self.imgSize)


    def mkImg(self, *larg, **karg):

        if karg.has_key('rand') and karg['rand'] == True:
            rand = True
        else:
            rand = False

        if karg.has_key('mode') and karg['mode'] == 'yang':
            self.iDir = os.path.join(self.baseDir, 'yang')
            self.isYang = True
        elif karg.has_key('mode') and karg['mode'] == 'yin':
            self.iDir = os.path.join(self.baseDir, 'yin')
            self.isYang = False

        if rand:
            imgFile = os.path.join(self.iDir, self.imgDir[randint(0, len(self.imgDir) - 1)])
        else:
            if self.img[1].has_key('iSeq'):
                # print 'iSeq: {}'.format(self.img[1]['iSeq'])
                if self.img[1]['iSeq'] < len(self.imgDir):
                    imgFile = os.path.join(self.iDir, self.imgDir[self.img[1]['iSeq']])
                else:
                    imgFile = os.path.join(self.iDir, self.imgDir[0])
            else:
                imgFile = os.path.join(self.iDir, self.imgDir[0])

        ### Make sure the image file exists.
        ## if so create the image and get texture size
        if os.path.exists(imgFile):
            # src = "http://placehold.it/480x270.png&text=slide-%d&.png" % i
            # image = AsyncImage(source=src, allow_stretch=True)

            img = Image(source=imgFile)
            iSize = img.texture_size
            fName = os.path.splitext(imgFile)[0]
            seqNum = int(fName.split('_')[1])

            self.img = (img, {'size': iSize, 'iSeq': seqNum, 'isYang': self.isYang})
            # pP(self.img[1].items())
            self.imgSize(self, self.size)

        else:
            return False


    def play(self, mod='yang'):
        if not self.playing:
            self.playing = True
        print 'playing ...'
        self.anim = Clock.schedule_interval(partial(self.mkImg, 'mode', mod), 0.058)


    def stop(self):
        if self.playing:
            self.playing = False
        print 'Stoped...'
        # unschedule using Clock.unschedule
        Clock.unschedule(self.anim)


    def imgSize(self, obj, val):
        if self.height > self.img[1]['size'][0]:
            self.canvas.before.clear()
            with self.canvas.before:
                Color(1, 1, 1, 1)
                Rectangle(texture=self.img[0].texture, height=self.img[1]['size'][1], pos=self.pos)
        else:
            self.canvas.before.clear()
            with self.canvas.before:
                Color(1, 1, 1, 1)
                Rectangle(texture=self.img[0].texture, size=self.size, pos=self.pos)


    def on_enter(self, *arg1):
        if self.isBtn:
            self.play({'mode': 'yin'})


    def on_leave(self, *arg1):
        if self.isBtn:
            self.stop()


class juicePop(juicyPopUp):
    ''' jiucePop() is a class of popup that will display information and 
        require a response from the user in the form of yes/no. This class 
        also is able to do non conformation popups as well. '''
    def __init__(self, *args, **kwargs):
        super(juicePop, self).__init__(*args, **kwargs)
        # print kwargs

        ### popup settings ###
        ## we modify the juicyPopUp() defaults
        self.rSize = (0.75, 0.5)

        ## Overide defauld position of the title alignment to center.        
        self.title_align = 'center'

        ### we will set this later with a concfig file option
        # self.title_font = 'Font Name here'
        
        ## title font size
        self.title_size = 25
    
        #######################

        ### Here we get the kwarg 'ask' 
        ## or set to false for logic later
        self.ask = False if not kwargs.has_key('ask') else kwargs['ask']

        ### Set the kwarg 'msg' or set to Unknone Error
        self.msg = 'Unknone Error: ' if not kwargs.has_key('msg') else kwargs['msg']

        ### Create a new layout for this class
        self.box = FloatLayout(size_hint=(None, None))

        ### Create a label and bind its size to its texture size.
        self.retXT = Label(text=self.msg, size_hint=(None, None))
        self.retXT.bind(texture_size=self.retXT.setter('size'))

        ### Do logic to set button functionality.
        if self.auto_dismiss is False:
            ## if auto dismiss is false
            ## create buttons for dismissal.
            if not self.ask:
                ### If ask is false create only one button.
                ## used for an anouncement that needs fonformation
                
                ## create ok button and bind it to juiceOut() function
                self.ok = juiceBtn(text='Ok', size_hint=(None, None))
                self.ok.bind(on_press=self.juiceOut)

                ## add the button to the container
                self.box.add_widget(self.ok)

            else:
                ### If ask is true create two buttons.
                ## used for yes no functionality
                
                ## create ok button and bind it to juiceOut() function
                self.ok = juiceBtn(text='Ok', size_hint=(None, None))
                self.ok.bind(on_press=self.juiceOut)
                ## add the button to the container
                self.box.add_widget(self.ok)

                ## create no button and bind it to juiceOut() function
                self.no = juiceBtn(text='No', size_hint=(None, None))
                self.no.bind(on_press=self.juiceOut)
                ## add the button to the container
                self.box.add_widget(self.no)

        else:
            ### The class is set to auto dismiss 
            ## set the background color to black
            ## and opacity to 20%
            self.background_color = (0, 0, 0, .2)

        ## add the return message to the container.S
        self.box.add_widget(self.retXT)

        ## add the layout to the popup.content
        self.content = self.box

        ## position the elements with mkPos() function
        self.cardSize()
        self.mkPos()

        ### Unbind and rebind the winSize() function
        ## to winSize2()
        self.unbind(pos=self.winSize, size=self.winSize)
        self.bind(pos=self.winSize2, size=self.winSize2)


    def mkPos(self):
        ''' This function positions all the elements created in the class '''
        ## Ser=t the size/position of the content
        self.box.size  = self.size        
        self.box.pos = ((Window.size[0] - self.width) * .5, (Window.size[1] - self.height) * .5)

        ## Set size and position of the return message
        self.retXT.size = (self.box.width * .9 , self.box.height * .1)
        self.retXT.pos_hint = {'center_x': .5, 'center_y': .5}
        self.retXT.font_size = 22
        
        ## Handle the autodismiss logic and ask logic and build their 
        ## positions accordingly
        if self.auto_dismiss is False:
            if not self.ask:
                self.ok.size = (self.box.width * .75, self.box.height * .25)
                self.ok.pos_hint = {'center_x': .5, 'center_y': .18}
            else:
                self.ok.size = (self.box.width * .37, self.box.height * .25)
                self.ok.pos_hint = {'center_x': .29, 'center_y': .18}

                self.no.size = (self.box.width * .37, self.box.height * .25)
                self.no.pos_hint = {'center_x': .69, 'center_y': .18}


    def winSize2(self, obj, siz):
        ''' This function resizes the content bound to the window size '''
        ## position the background
        self.cardSize()
        ## position the elements created in this class.
        self.mkPos()


    def juiceOut(self, tgt):
        ''' This function sets is triggered when the user selects and
            returns a Boolean: true/false for the question asked. If not
            ask is true will return true. '''

        ### Keep this. We may need to do this in the future?
        # self.punch_btn.text = '{} punches to be added.'.format(tgt.id.split('_')[0])
        # unbind all the buttons!
        # for btn in self.punchesGrid.walk():
        #     print btn.id
        #     btn.unbind(on_press=self.addPunches)
        # self.punches = int(tgt.id.split('_')[0])
        # self.ok.unbind(on_press)
        
        ### Set ask to boolelan.
        ## Access the value with self.ask: where self is the instance 
        ## of this class.
        self.ask = False if tgt.text == 'no' else True
        print 'ask: {}'.format(self.ask)
        self.dismiss()


##############################################
########### This is the spiceGrid ############
##############################################


class spiceGrid(ButtonBehavior, Widget, HoverBehavior):
    def __init__(self, *arg1, **args):
        super(spiceGrid, self).__init__(*arg1, **args)

        self.cols = 0
        self.col_width = self.width

        self.bind(size=self.winSize)
        # self.bind(pos=self.winPos)

        # --- make Juice ID Labels and container
        self.juiceBox = spiceLBL(id='jBox', juice_id=args['juice_id'])

        # --- make First Name ID Labels and container
        self.fNameBox = spiceLBL(id='fName', first_name=args['first_name'])

        # --- make Last Name ID Labels and container
        self.lNameBox = spiceLBL(id='lName', last_name=args['last_name'])

        with self.canvas.before:
            self.spiColor = Color(0.2, 0.7, 0.4, 0.8)
            self.spicRect = RoundedRectangle(size=self.size, pos=self.pos, radius=[10, 10, 10, 10])

        # Add containders
        self.add_widget(self.juiceBox)

        self.add_widget(self.fNameBox)
        self.add_widget(self.lNameBox)

        self.bind(on_enter=self.on_enter)
        self.bind(on_leave=self.on_leave)

        self.mkPos()

    def mkPos(self):
        self.col_width = self.width / self.cols

        self.canvas.before.clear()
        with self.canvas.before:
            self.spiColor = Color(0.2, 0.7, 0.4, 0.8)
            self.spicRect = RoundedRectangle(size=self.size, pos=self.pos, radius=[10, 10, 10, 10])

        for x, child in enumerate(self.children):
            child.height = self.height
            child.width = self.col_width
            child.x = self.x + (x * self.col_width)
            child.y = self.y

    def winSize(self, obj, siz):
        self.size = siz
        self.mkPos()

    def winPos(self, obj, pos):
        self.pos = pos
        self.mkPos()

    def add_widget(self, widget):
        super(spiceGrid, self).add_widget(widget)
        self.cols += 1
        self.col_width = self.width / self.cols
        self.mkPos()

    def remove_widget(self, widget, isFrun):
        super(spiceGrid, self).remove_widget(widget)
        self.cols -= 1
        self.mkPos()

    def on_enter(self, *arg1):
        self.canvas.before.clear()
        with self.canvas.before:
            self.spiColor = Color(0.3, 0.1, 0.9, 0.5)
            self.spicRect = RoundedRectangle(size=self.size, pos=self.pos, radius=[10, 10, 10, 10])

    def on_leave(self, *arg1):
        self.canvas.before.clear()
        with self.canvas.before:
            self.spiColor = Color(0.2, 0.7, 0.4, 0.8)
            self.spicRect = RoundedRectangle(size=self.size, pos=self.pos, radius=[10, 10, 10, 10])


class spiceLBL(Label):
    def __init__(self, **args):
        super(spiceLBL, self).__init__(**args)

        self.bind(size=self.winSize, pos=self.winPos)
        self.bind()

        self.key = ''
        self.val = ''

        for k, v in args.items():
            if 'juice_id' in k or 'last_name' in k or 'first_name' in k or 'date_started' in k or 'date_expires' in k:
                k = k.split('_')
                self.key = ' {} {}: '.format(k[0].capitalize(), k[1])
                if not k[0] == 'date':
                    self.val = v.capitalize()
                else:
                    # we are date, do cool stuf here!
                    self.val = '{}/{}/{}'.format(v.day, v.month, v.year)

        self.juicyLBL = Label(text=self.key, size_hint=(None, None))
        self.juicyLBL.bind(texture_size=self.juicyLBL.setter('size'))

        self.jID = Label(text=str(self.val), size_hint=(None, None))
        self.jID.bind(texture_size=self.jID.setter('size'))

        self.add_widget(self.juicyLBL)
        self.add_widget(self.jID)

        self.mkSpiceLBL()

    def mkSpiceLBL(self, *args):

        self.juicyLBL.size = (self.width * 0.3, self.height * .5)
        self.juicyLBL.pos = (self.x + ((self.width * 0.3) * 0.25), self.y)

        self.juicyLBL.font_size = int(self.juicyLBL.height * .5)#  if self.juicyLBL.width > self.juicyLBL.height else self.juicyLBL.width * .5)

        self.juicyLBL.texture_update()

        while (self.juicyLBL.size[0]) > (self.width * .3):
            self.juicyLBL.size[0] = self.juicyLBL.size[0] - 15
            self.juicyLBL.font_size = self.juicyLBL.font_size - 1
            self.juicyLBL.texture_update()

        self.jID.size = (self.width * 0.7, self.height * .8)
        self.jID.pos = (self.juicyLBL.x + self.juicyLBL.width + (self.juicyLBL.width * .20), self.juicyLBL.pos[1])

        self.jID.font_size = int(self.jID.height * .7)# if self.jID.width > self.jID.height else self.jID.height * .4)

        self.jID.texture_update()

        while (self.jID.size[0]) > (self.width * 0.7) - 50:
            self.jID.size[0] = self.jID.size[0] - 15
            self.jID.font_size = self.jID.font_size - 1
            self.jID.texture_update()

        self.juicyLBL.canvas.before.clear()
        with self.juicyLBL.canvas.before:
            self.spiColor = Color(0.2, 0.8, 0.3, 0.6)
            self.spicRect = RoundedRectangle(size=(self.juicyLBL.size[0] + 10, self.juicyLBL.size[1]), pos=(self.juicyLBL.pos[0] - 5, self.juicyLBL.pos[1]), radius=[10, 10, 10, 10])

    def winSize(self, obj, siz):
        self.size = siz
        self.mkSpiceLBL()

    def winPos(self, obj, pos):
        self.pos = pos
        self.mkSpiceLBL()

    def textureSize(self, obj, siz):
        obj.texture_size = siz if siz[0] > self.size[1] else (siz[1], siz[0])


class mkUserSearchBuilder(FloatLayout):
    '''This class builds the user interface for the customer search'''
    def __init__(self, **args):
        super(mkUserSearchBuilder, self).__init__(**args)
        self.size_hint = (None, None)

        # self.iBoxH = Window.size[1] * .14
        # self.iBoxW = Window.size[0] * .60
        # self.spacer = Window.size[1] * .03

        # Build First Name Search Field
        self.fNameBX = juicyInput(lable_text='First Name')
        self.fNameBX.id = 'fNameBX'
        self.add_widget(self.fNameBX)

        # Build Last Name Search Field
        self.lNameBX = juicyInput(lable_text='Last Name')
        self.lNameBX.id = 'lNameBX'
        self.add_widget(self.lNameBX)

        # Build User ID Search Field
        self.juiceIdBX = juicyInput(lable_text='Juice ID')
        self.juiceIdBX.id = 'juiceIdBX'
        self.add_widget(self.juiceIdBX)

        # Build Search Button
        self.search_btn = juiceBtn()
        self.search_btn.id = 'search_btn'
        self.search_btn.text = 'Search'
        self.search_btn.size_hint = (None, None)
        self.search_btn.size = (Window.size[0] * .3, Window.size[1] * .3)
        self.search_btn.pos_hint = {'right': 1, 'bottom': 1}

        # self.search_btn.bind(on_press=self.juicerSearch)
        self.add_widget(self.search_btn)
        self.bind(size=self.winSize)
        self.mkSize()

    # def juicerSearch(self, tgt):
    #     print 'hi you did it!: {} --> (fName_txt.text: {}, lName_txt.text: {}, userID_txt.text: {})'.format(tgt.id, self.fNameBX.juicy_txt.text, self.lNameBX.juicy_txt.text, self.juiceIdBX.juicy_txt.text)
    #     
    def mkSize(self):
        iBoxH = Window.size[1] * .14
        iBoxW = Window.size[0] * .60
        spacer = Window.size[1] * .03
        self.fNameBX.pos = (10, ((self.height - iBoxH * 1.5) - spacer))
        self.fNameBX.size = (iBoxW, iBoxH)
        self.lNameBX.pos = (10, (self.fNameBX.y - iBoxH) - spacer)
        self.lNameBX.size = (iBoxW, iBoxH)
        self.juiceIdBX.pos = (10, (self.lNameBX.y - iBoxH) - spacer)
        self.juiceIdBX.size = (iBoxW, iBoxH)

    def winSize(self, obj, siz):
        self.mkSize()

    def clearTxt(self, obj):
        obj.juicy_txt.text = ''

    def clearAllTxt(self):
        for v in self.children:
            if v.typeOf == 'txt':
                v.juicy_txt.text = ''


class mkUserAdd(mkUserSearchBuilder):
    '''This class builds the user interface for the customer add function.'''
    def __init__(self, **args):
        super(mkUserAdd, self).__init__(**args)

        self.noOfPunches = 5

        # Build Email Search Field
        self.emailBX = juicyInput(lable_text='Email')
        self.add_widget(self.emailBX)

        # self.juiceIdBX.pos = (10, (self.emailBX.y - self.iBoxH) - self.spacer)

        # Build Punches Search Button
        self.punches_btn = juiceBtn()
        self.punches_btn.text = '{}'.format(self.noOfPunches)
        self.punches_btn.size_hint = (None, None)
        self.punches_btn.size = (300, 90)
        self.punches_btn.pos_hint = {'right': .7, 'bottom': 1}
        self.punches_btn.bind(on_press=self.punchSelector)
        self.add_widget(self.punches_btn)

        # Build The Punch Selection Button Poppup

        self.pGrid = selectPunchsToAdd(auto_dismiss=False, punchTxt=self.punches_btn)
        # self.pGrid.auto_dismiss = False
        self.pGrid.title = 'Select Punches To Add'
        self.pGrid.size_hint = (None, None)
        # self.pGrid.size = (Window.size[0] * .4, Window.size[1] * .5)

        # Build Search Button
        self.search_btn.id = 'addUser_btn'
        self.search_btn.text = 'Add Juicer'

        self.mkSize2()

        # self.search_btn.unbind(on_press=self.juicerSearch)

        # self.search_btn.bind(on_press=self.addJuicer)


    def winSize(self, obj, siz):
        self.mkSize2()


    def mkSize2(self):
        # super(mkUserAdd, self).mkSize()
        # self.emailBX.pos = (10, (self.lNameBX.y - 110) - 25)
        # self.juiceIdBX.pos = (10, (self.emailBX.y - 110) - 25)

        self.iBoxH = Window.size[1] * .14
        self.iBoxW = Window.size[0] * .60
        self.spacer = Window.size[1] * .03
        self.fNameBX.pos = (10, ((self.height - self.iBoxH * 1.5) - self.spacer))
        self.fNameBX.size = (self.iBoxW, self.iBoxH)
        self.lNameBX.pos = (10, (self.fNameBX.y - self.iBoxH) - self.spacer)
        self.lNameBX.size = (self.iBoxW, self.iBoxH)

        self.emailBX.pos = (10, (self.lNameBX.y - self.iBoxH) - self.spacer)

        self.juiceIdBX.pos = (10, (self.emailBX.y - self.iBoxH) - self.spacer)
        self.juiceIdBX.size = (self.iBoxW, self.iBoxH)


    # def addJuicer(self, tgt):
    #     print 'hi you did it!: {} --> (fName_txt.text: {}, lName_txt.text: {}, userID_txt.text: {}, email_txt.text: {}, Punches to add: {})'.format(tgt.id, self.fNameBX.juicy_txt.text, self.lNameBX.juicy_txt.text, self.juiceIdBX.juicy_txt.text, self.emailBX.juicy_txt.text, self.noOfPunches)

    def punchSelector(self, tgt):
        self.pGrid.bind(on_dismiss=self.punchSelected)
        self.pGrid.open()

    def punchSelected(self, tgt):
        self.noOfPunches = self.pGrid.punches


class juiceBtn(Button, HoverBehavior):
    def __init__(self, **kwargs):
        super(juiceBtn, self).__init__(**kwargs)

        self.typeOf = 'btn'

        self.font_size = 25
        self.font_name = 'FreeSans'
        # self.text = 'Button'
        self.pos_hint = {'center_x': 0.5, 'center_y': 0.5}

        self.background_normal = os.path.join(imgDir, 'blank.png')
        # self.background_color = (0.3, 0.9, 0.5, 0.90)

        self.bind(pos=self.btnSize, size=self.btnSize)

        with self.canvas.before:
            self.btnColor = Color(0.2, 0.60, 0.30, 0.9)
            self.bg = RoundedRectangle(size=self.size, pos=self.pos, radius=[10, 10, 10, 10])

    def on_enter(self, *arg1):
        self.canvas.before.clear()
        with self.canvas.before:
            self.btnColor = Color(0.3, 0.1, 0.9, 0.9)
            self.bg = RoundedRectangle(size=self.size, pos=self.pos, radius=[10, 10, 10, 10])

    def on_leave(self, *arg1):
        self.canvas.before.clear()
        with self.canvas.before:
            self.btnColor = Color(0.2, 0.60, 0.30, 0.9)
            self.bg = RoundedRectangle(size=self.size, pos=self.pos, radius=[10, 10, 10, 10])

    def btnSize(self, obj, val):
        obj.bg.size = obj.size
        obj.bg.pos = obj.pos


class juicyInput(FloatLayout):
    def __init__(self, **kwargs):
        super(juicyInput, self).__init__(**kwargs)

        self.typeOf = 'txt'
        self.box = BoxLayout()
        self.box.size_hint = (None, None)
        self.box.size = self.size
        self.box.pos = self.pos
        self.box.orientation = 'vertical'
        self.box.padding = [10]

        self.label_text = kwargs['lable_text']

        # Build Lable
        self.juicy_lbl = Label(text='{}: '.format(self.label_text))
        self.juicy_lbl.size_hint = (None, None)
        # self.juicy_lbl.size = (100, (self.height * .5) - 10)
        self.juicy_lbl.bold = True
        self.juicy_lbl.color = (0, 0, 0, 1)
        self.juicy_lbl.font_size = int(self.juicy_lbl.height * .5)
        # self.juicy_lbl.pos_hint = {'x': .07, 'center_y': .5}

        self.box.add_widget(self.juicy_lbl)

        # Build Text Input
        self.juicy_txt = juicyTextInput()
        self.juicy_txt.size_hint = (None, None)
        # self.juicy_txt.size = (self.width - (self.width * .1), (self.height * .5) - 10)
        self.juicy_txt.font_size = self.juicy_txt.size[1] * .60
        # self.juicy_txt.pos_hint = {'center_x': .5, 'center_y': .5}

        self.box.add_widget(self.juicy_txt)

        self.add_widget(self.box)

        self.juicy_txt.bind(size=self.inText)

        self.bind(pos=self.posChange, size=self.posChange)

        self.mkSize()


    def mkSize(self):
        self.box.size = self.size
        self.box.pos = self.pos
        self.juicy_lbl.size = (self.width * .9, (self.height * .5) - 10)
        # self.juicy_lbl.pos_hint = {'x': 0, 'center_y': .5}
        self.juicy_lbl.pos = (0, self.height * .5)
        self.juicy_txt.size = (self.width * .9, (self.height * .5) - 10)
        # self.juicy_txt.pos_hint = {'center_x': .5, 'center_y': .5}
        self.juicy_txt.pos = ((self.width - self.juicy_txt.width) * .5, 0)
        
        with self.box.canvas.before:
            Color(0.3, 0.9, 0.5, 0.90)
            RoundedRectangle(size=self.size, pos=self.pos, radius=[10, 10, 10, 10])


    def inText(self, obj, siz):
        print self, obj, siz
        # while len(self.label_text) %2 == 0:
        #     obj.font_size = obj.font_size - 1
        #     print 'here'
        while (self.juicy_txt.size[0]) > (self.width * .95):
            print 'here goToHere'
            # self.juicy_txt.size[0] = self.juicy_txt.size[0] - 15
            self.juicy_txt.font_size = int(self.juicy_txt.font_size - 1)
            self.juicy_txt.texture_update()


    def posChange(self, obj, pos):
        self.mkSize()


class juicyTextInput(TextInput):
    '''Thiis class redifines the TextInput and adds a label insted of the text.'''
    def __init__(self, **args):
        super(juicyTextInput, self).__init__(**args)
        self.write_tab = False
        self.multiline = False
        self.background_color = (1, 1, 1, .8)
        self.border = (4, 4, 4, 4)
        # self.font_name
        self.font_size = int(self.size[1] * .28)


class spiceJuice(object):

    juiceIt = theJuice()

    names = ['Austin', 'Nancy', 'Beau', 'Kathren', 'Abby', 'Lynda', 'Gale', 'Taylor', 'Michelle', 'Michael', 'Lydia', 'Jacqueline', 'Bishop', 'Alex', 'Cupper', 'Love', 'Caitlyn', 'Aiden', 'Carney', 'Kerven', 'Oakley', 'Shaun', 'Kelly', 'Turner', 'AppleBum', 'TeekyFreeky', 'Allimode', 'Hampshire', 'Ravenwood', 'Eliswitch', 'Huro']
    for i, lName in enumerate(names):#[:5]):
        for j, name in enumerate(names):#[len(names) - 5:]):
            i = i + 13
            j = (j + 1) * i
            juicer = cAttrs
            juicer['juice_id'] = '{}'.format(i*j)
            juicer['first_name'] = name
            juicer['last_name'] = lName
            juicer['email'] = '{}.{}@yahoo.com'.format(name, lName)
            juicer['punches'] = i

            # ret = juiceIt.mixItUP(**juicer)

            # if ret is False:
            #     print '\nFUCK\n'
            # else:
            #     print 'success: {}'.format(ret)


class mainBG(Widget):
    def __init__(self, **kwargs):
        super(mainBG, self).__init__(**kwargs)
        self.size_hint = (None, None)
        self.size = Window.size

        self.pos_hint = {'left': 1, 'top': 1}
        self.bg_path = os.path.join(os.getcwd(), 'img', 'L00K.jpg')
        self.logo_path = os.path.join(os.getcwd(), 'img', 'juicyLogo.png')

        if os.path.exists(self.bg_path):
            # src = "http://placehold.it/480x270.png&text=slide-%d&.png" % i
            # image = AsyncImage(source=src, allow_stretch=True)
            self.bgImg = Image(source=self.bg_path)
            self.bgImgSize = pImage.open(self.bg_path).size
            print 'BG size: {}'.format(self.bgImg.texture_size)
        else:
            print 'this is not the path you seek! : {}'.format(self.bg_path)

        if os.path.exists(self.logo_path):
            self.logoImg = Image(source=self.logo_path)
            self.logoSize = pImage.open(self.logo_path).size
            # self.add_widget(self.logoImg)
            print 'LOGO size: {}'.format(self.logoImg.texture_size)
        else:
            print 'this is not the path you seek! : {}'.format(self.logo_path)

        self.imgRsize()
        self.bind(size=self.update_bg)

    def update_bg(self, obj, siz):
        self.size = siz
        self.imgRsize()

    def imgRsize(self):
        # availWidth = self.width - self.startPage.userAdd.fNameBX.width
        availWidth = self.width - 530

        if availWidth >= self.logoSize[0]:
            logoW = self.logoSize[0]
            logoH = self.logoSize[1]
        else:
            per = float(availWidth) / self.logoSize[0]
            logoW = self.logoSize[0] * per
            logoH = self.logoSize[1] * per

        self.canvas.before.clear()
        with self.canvas.before:
            self.bg = Rectangle(texture=self.bgImg.texture, pos=self.pos, size=self.size)
            self.logo = Rectangle(texture=self.logoImg.texture, size=(logoW, logoH), pos=(self.width - logoW - 5, self.height - logoH - 20))


class blender(FloatLayout):
    def __init__(self, **kwargs):
        super(blender, self).__init__(**kwargs)
        self.size_hint = (None, None)
        self.size = Window.size
        self.bind(size=self.winSize)

        self.bg = mainBG()
        self.bg.size = self.size
        self.add_widget(self.bg)

        self.startPage = startPage()
        self.add_widget(self.startPage)

        self.powBar = powerNavBar()
        self.add_widget(self.powBar)

    def winSize(self, obj, siz):
        self.bg.size = siz
        self.startPage.size = siz


class mkJuice(App):
    def __init__(self, *args, **kwargs):
        super(mkJuice, self).__init__(*args, **kwargs)
        # Window.size = (1024, 768)
        Window.size = (800, 600)
        Window.clearcolor = (0, .255, 0, 1)

        if kwargs.has_key('button_color_normal'):
            btnColNorm = kwargs['button_color_normal']

        self.size_hint = (None, None)
        self.size = Window.size
        self.pos = (0, Window.height - self.size[1])

        self.blend = blender()
        self.blend.size_hint = (None, None)
        self.blend.size = self.size
        self.blend.pos = (0, Window.height - self.size[1])

        addUserConfirm(me='austin@gmail.com', to='austin@3ch0peak.com')

        Window.bind(on_resize=self.winReSize)
        # Window.bind(on_motion=self.onMotion)

    def build(self):
        return self.blend

    def winReSize(self, obj, wdth, hght):
        self.size = (wdth, hght)
        self.blend.size = (wdth, hght)
        # print 'Resizing:\n  Object: {}\n  Width: {}, Height: {}\n'.format(obj, wdth, hght)

    def onMotion(self, eType, evT, ball):
        print 'type: {}, event: {}, ball: {}'.format(eType, evT, ball)


class addUserConfirm(object):
    def __init__(self, **args):
        pass
        #!/usr/bin/env python
        # import smtplib
        # from email.mime.multipart import MIMEMultipart
        # from email.mime.text import MIMEText

        # me = "my@email.com"
        # you = "your@email.com"

        # # Create message container - the correct MIME type is multipart/alternative.
        # msg = MIMEMultipart('alternative')
        # msg['Subject'] = "Link"
        # msg['From'] = args['me']
        # msg['To'] = args['to']

        # # Create the body of the message (a plain-text and an HTML version).
        # text = "Hi!\nHow are you?\nHere is the link you wanted:\nhttps://www.python.org"
        # html = """\
        # <html>
        #   <head></head>
        #   <body>
        #     <p>Hi!<br>
        #        How are you?<br>
        #        Here is the <a href="https://www.python.org">link</a> you wanted.
        #     </p>
        #   </body>
        # </html>
        # """

        # soup = BS('', 'lxml')
        # soup['head'] = ''
        # soup['body'] = ''
        # soup['body']['style']['background-color'] = '#00CCFF'
        # soup['body']['p'] = 'Hi!<br>How are you?<br>Here is the <a href="https://www.python.org">link</a> you wanted.'

        # print soup.prettify()

        # # Record the MIME types of both parts - text/plain and text/html.
        # part1 = MIMEText(text, 'plain')
        # part2 = MIMEText(html, 'html')

        # # Attach parts into message container.
        # # According to RFC 2046, the last part of a multipart message, in this case
        # # the HTML message, is best and preferred.
        # msg.attach(part1)
        # msg.attach(part2)

        # # Send the message via local SMTP server.
        # s = smtplib.SMTP('localhost')
        # # sendmail function takes 3 arguments: sender's address, recipient's address
        # # and message to send - here it is sent as one string.
        # # s.sendmail(me, you, msg.as_string())
        # # s.quit()


def sortDirs(x):
    ''' This function is for sorting the diredtory images in numerical order. '''
    xNum = os.path.splitext(x)[0].split('_')[1]
    return int(xNum)


def showWidget(self, col=False):
    print u'id: {}\nsize: {}\npos: {}\n'.format(self.id, self.size, self.pos)
    with self.canvas.before:
        if col != False:
            Color(*col)
        else:
            Color(0.2, 0.3, 0.7, 4.0)
        Rectangle(size=self.size, pos=self.pos)


def mkArgs(dlArgs):
    if len(dlArgs) > 1:
        lt, dt = [], {}
        for ar in dlArgs[1:]:
            if '=' in ar:
                a = ar.split('=')
                dt[a[0]] = a[1]
            else:
                lt.append(ar)
        return (lt, dt)
    else:
        return None


if __name__ == '__main__':
    args = mkArgs(sys.argv)
    if args is None:
        juiceIT = mkJuice(size=Window.size)
    else:
        juiceIT = mkJuice(*args[0], **args[1])
    juiceIT.run()
