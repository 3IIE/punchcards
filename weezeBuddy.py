import re
from collections import OrderedDict as OD
# import mysql.connector
from email_validator import validate_email, EmailNotValidError
from mysql import connector

import time

from datetime import date, datetime, timedelta

class theJuice(object):
    ## https://dev.mysql.com/doc/connector-python/en/connector-python-examples.html
    def __init__(self):

        self.fNameRgX = re.compile(r'^[A-Za-z]{1,35}$')
        self.lNameRgX = re.compile(r'^[A-Za-z]{1,22}$')
        self.juiceIdRgX = re.compile(r'^[A-Za-z0-9]{1,20}$')
        self.punchesRgX = re.compile(r'^[0-9]{0,2}$')

        self.creds = {'user': 'juiceBar', 'database': 'juice_bar', 'password': 'weezeTheJuiceBuddy#33', 'host': '127.0.0.1', 'raise_on_warnings': True}

        self.cAttrs = OD([('id', ''), ('juice_id', ''), ('first_name', ''), ('last_name', ''), ('email', ''), ('punches', ''), ('date_started', ''), ('date_expires', '')])


    def blend(self):
        try:
            cnx = connector.connect(**self.creds)
        except connector.Error as err:
            if err.errno == connector.errorcode.ER_ACCESS_DENIED_ERROR:
                return "Something is wrong with your user name or password"
            elif err.errno == connector.errorcode.ER_BAD_DB_ERROR:
                cnx2 = self.prepFood()
                # print 'conx2'
                return (False, cnx2)
            else:
                return (False, err)
        else:
            # print 'conx1'
            return cnx


    def prepFood(self):
        ## https://dev.mysql.com/doc/connector-python/en/connector-python-example-ddl.html

        cred = self.creds
        del cred['database']

        cnx = connector.connect(**cred)
        cursor = cnx.cursor()

        buildb = OD()

        dbName = 'juice_bar'

        buildb['juice_cards'] = ('''CREATE TABLE juice_cards (id int(111) AUTO_INCREMENT PRIMARY KEY UNIQUE,
            juice_id varchar(50) NOT NULL,
            first_name varchar(50) NOT NULL,
            last_name varchar(50) NOT NULL,
            email varchar(50) NOT NULL,
            punches int(30) NOT NULL,
            date_started date NOT NULL,
            date_expires date NOT NULL ) ENGINE=InnoDB''')

        buildb['juice_hist'] = ('''CREATE TABLE juice_hist (id int(11) PRIMARY KEY NOT NULL,
            last_punches date NOT NULL) ENGINE=InnoDB''')

        buildb['juicers'] = ('''CREATE TABLE juicers (id int(11) PRIMARY KEY NOT NULL UNIQUE,
            j_name varchar(50) NOT NULL) ENGINE=InnoDB''')

        buildb['mods'] = ('''CREATE TABLE juice_mods (mod_id int(11) PRIMARY KEY NOT NULL UNIQUE,
            mod_date DATETIME NOT NULL,
            j_name varchar(50) NOT NULL,
            notes MEDIUMTEXT NOT NULL) ENGINE=InnoDB''')

        buildb['purchase_hist'] = ('''CREATE TABLE purchase_hist (id int(11) PRIMARY KEY NOT NULL,
            recent_purc date NOT NULL) ENGINE=InnoDB''')

        buildb['juice_admin'] = ('''CREATE TABLE juice_admin (id int(1) PRIMARY KEY NOT NULL UNIQUE,
            user varchar(60) NOT NULL,
            pass varchar(22) NOT NULL) ENGINE=InnoDB''')

        try:
            cursor.execute("CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(dbName))

            try:
                cnx.database = dbName
            except connector.Error as err:
                if err.errno == connector.errorcode.ER_BAD_DB_ERROR:
                    return 'Database has not been built.'
                else:
                    print 'mysqlERROR: {}'.format(err)
                    return err

            for key, val in buildb.iteritems():
                try:
                    print 'Trying to create table: {}'.format(key)
                    cursor.execute(val)
                except connector.Error as err:
                    if err.errno == connector.errorcode.ER_TABLE_EXISTS_ERROR:
                        print "Table already exists."
                    else:
                        print err.msg
                else:
                    print "Table Created."

        except connector.Error as err:
            return "Failed creating database: {}".format(err)

        cursor.close()
        print 'Connection: Success, Database has been created and is ready for data.'
        cnx.close()
        self.creds['database'] = 'juice_bar'
        cnx2 = connector.connect(**self.creds)
        return cnx2


    def mixItUP(self, **args):
        ## https://dev.mysql.com/doc/connector-python/en/connector-python-example-cursor-transaction.html

        # print args

        blendIt = self.blend()

        if isinstance(blendIt, tuple) and blendIt[0] is False:
            return blendIt

        cursor = blendIt.cursor()

        today = datetime.now().date()
        expires = datetime.now().date() + timedelta(days=546)

        # self.checkFruit(args)

        args['date_started'] = today
        args['date_expires'] = expires

        if args.has_key('juice_id') and args.has_key('first_name') and args.has_key('last_name') and args.has_key('email') and args.has_key('punches'):

            chkFruit = self.checkFruit(args)
            if chkFruit[0] is False:
                return (False, chkFruit)

            checkJuice = self.mixItDN(**args)

            print checkJuice

            if checkJuice[0] is not False:
                return (False, checkJuice)


            ##   juice_cards   ##
            ###   juice_id | varchar(50) | NOT NULL
            ###   first_name | varchar(50) | NOT NULL
            ###   last_name | varchar(50) | NOT NULL
            ###   email | varchar(50) | NOT NULL
            ###   punches | int(2) | NOT NUL
            ###   date_started | date | NOT NULL
            ###   date_expires | date | NOT NULL
            addJuiceCard = ('INSERT INTO juice_cards (juice_id, first_name, last_name, email, punches, date_started, date_expires) VALUES (%(juice_id)s, %(first_name)s, %(last_name)s, %(email)s, %(punches)s, %(date_started)s, %(date_expires)s)')

        # print args

        # Insert new employee
        cursor.execute(addJuiceCard, args)
        # emp_no = cursor.lastrowid

        # Make sure data is committed to the database
        blendIt.commit()

        cursor.close()
        blendIt.close()

        return (True, args)

        # print '{}, {}, {}, {}, {}, {}, {}'.format(args['juice_id'], args['first_name'], args['last_name'], args['email'], int(args['punches']), today, expires)


    def mixItDN(self, **args):
        self.usrCheck = False
        blendIt = self.blend()

        if isinstance(blendIt, tuple) and blendIt[0] is False:
            return blendIt

        cursor = blendIt.cursor()

        chkFruit = self.checkFruit(args)

        if chkFruit[0] is False:
            return chkFruit

        if args.has_key('first_name') and args.has_key('last_name') and args.has_key('juice_id'):

            # if self.checkFruit(args) is False:
            #     return False
            query = ("SELECT * FROM juice_cards WHERE %(first_name)s = first_name AND %(last_name)s = last_name AND %(juice_id)s = juice_id ORDER BY last_name desc")
            cursor.execute(query, args)

        elif args.has_key('first_name') and args.has_key('last_name'):
            # if self.checkFruit(args) is False:
            #     return False
            query = ("SELECT * FROM juice_cards WHERE %(first_name)s = first_name AND %(last_name)s = last_name ORDER BY last_name desc")
            cursor.execute(query, args)

        elif args.has_key('first_name') and args.has_key('juice_id'):
            # if self.checkFruit(args) is False:
            #     return False
            query = ("SELECT * FROM juice_cards WHERE %(first_name)s = first_name AND %(juice_id)s = juice_id ORDER BY first_name desc")
            cursor.execute(query, args)

        elif args.has_key('last_name') and args.has_key('juice_id'):
            # if self.checkFruit(args) is False:
            #     return False
            query = ("SELECT * FROM juice_cards WHERE %(last_name)s = last_name AND %(juice_id)s = juice_id ORDER BY last_name desc")
            cursor.execute(query, args)

        elif args.has_key('first_name'):
            # if self.checkFruit(args) is False:
            #     return False
            query = ("SELECT * FROM juice_cards WHERE %(first_name)s = first_name ORDER BY last_name desc")
            cursor.execute(query, (args))

        elif args.has_key('last_name'):
            # if self.checkFruit(args) is False:
            #     return False
            query = ("SELECT * FROM juice_cards WHERE %(last_name)s = last_name ORDER BY first_name desc")
            cursor.execute(query, args)

        elif args.has_key('juice_id'):
            # if self.checkFruit(args) is False:
            #     return False
            query = ("SELECT * FROM juice_cards WHERE %(juice_id)s = juice_id")
            cursor.execute(query, args)
        else:
            print 'here'
            return (False, 'No Data')

        juicers = OD()

        retKey = 'Results for'

        # print args

        for k, v in args.items():
            if '_' in k:
                k = k.split('_')
                retKey = '{} {} {}: {}, '.format(retKey, k[0].capitalize(), k[1].capitalize(), v)
            else:
                retKey = '{} {}: {}, '.format(retKey, k.capitalize(), v)

        i = 1
        for (uid, juice_id, first_name, last_name, email, punches, date_started, date_expires) in cursor:

            juicer = OD()
            juicer['id'] = uid
            juicer['juice_id'] = juice_id
            juicer['first_name'] = first_name
            juicer['last_name'] = last_name
            juicer['email'] = email
            juicer['punches'] = punches
            juicer['date_started'] = date_started
            juicer['date_expires'] = date_expires

            juicers[str(i)] = juicer
            i += 1

        cursor.close()
        blendIt.close()
        if len(juicers) == 0:
            print 'there'
            return (False, 'No Data.')
        else:
            juicers['query'] = retKey.rstrip(', ')
            return (True, juicers)

    def spice(self):
        pass


    def checkFruit(self, args):

        if args.has_key('first_name'):
            # print args['first_name']
            if re.match(self.fNameRgX, args['first_name']) is None:
                retMsg = 'Failed to insert data {}: {}'.format('First Name', args['first_name'])
                ret = (False, retMsg)
                return ret

        if  args.has_key('last_name'):
            # print args['last_name']
            if re.match(self.lNameRgX, args['last_name']) is None:
                ret = (False, 'Failed to insert data {}: {}'.format('Last Name', args['last_name']))
                return ret

        if args.has_key('juice_id'):
            # print args['juice_id']
            if re.match(self.juiceIdRgX, args['juice_id']) is None:
                ret = (False, 'Failed to insert data {}: {}'.format('Juice Id', args['juice_id']))
                return ret

        if args.has_key('email'):
            # print args['email']
            emailTest = self.validEmail(args['email'])
            # print emailTest
            if not emailTest == args['email']:
                retMsg = emailTest.replace('. ', '.\n')
                ret = (False, retMsg.strip())
                return ret

        if args.has_key('punches'):
            # print args['punches']
            if re.match(self.punchesRgX, str(args['punches'])) is None:
                ret = (False, 'Failed to insert data {}'.format(args['punches']))
                return ret
            else:
                args['punches'] = int(args['punches'])

        return (True, args)


    def addJuiceHist(self, **args):

        blendIt = self.blend()
        cursor = blendIt.cursor()

        today = datetime.now().date()
        expiration = datetime.now().date() + timedelta(days=546)

        if not args.has_key('juice_id'):
            return False
        else:
            if re.match(self.juiceIdRgX, args['juice_id']) is None:
                return False

        if not args.has_key('first_name'):
            return False
        else:
            if re.match(self.fNameRgX, args['first_name']) is None:
                return False

        if not args.has_key('last_name'):
            return False
        else:
            if re.match(self.lNameRgX, args['last_name']) is None:
                return False

        ##   juice_hist   ##
        ###   id int(11) | PRIMARY | KEY NOT NULL
        ###   last_punches | date | NOT NULL
        addJuiceHist = ('''INSERT INTO juice_hist (id, last_punches) VALUES (%(emp_no)i, %(salary)s)''')

        # Insert new employee
        cursor.execute(addJuiceHist)

        # emp_no = cursor.lastrowid

        # Make sure data is committed to the database
        blendIt.commit()

        cursor.close()
        blendIt.close()


    def addAjuicer(self, **args):

        blendIt = self.blend
        cursor = blendIt.cursor()

        today = datetime.now().date()
        expiration = datetime.now().date() + timedelta(days=546)

        if not args.has_key('juice_id'):
            return False
        else:
            if re.match(self.juiceIdRgX, args['juice_id']) is None:
                return False

        if not args.has_key('first_name'):
            return False
        else:
            if re.match(self.fNameRgX, args['first_name']) is None:
                return False

        ##   juicers   ##
        ###   id int(11) PRIMARY KEY NOT NULL UNIQUE
        ###   j_name varchar(50) NOT NULL
        addJuicers = ('''INSERT INTO juicers (id, j_name) VALUES (%(emp_no)s, %(to_date)s)''')

        # Insert new employee
        cursor.execute(addJuicers)

        # emp_no = cursor.lastrowid

        # Make sure data is committed to the database
        blendIt.commit()

        cursor.close()
        blendIt.close()


    def modifyTheJuice(self, **args):

        blendIt = self.blend
        cursor = blendIt.cursor()

        today = datetime.now().date()
        expiration = datetime.now().date() + timedelta(days=546)

                ##   juice_mods   ##
        ###   mod_id int(11) PRIMARY KEY NOT NULL UNIQUE
        ###   mod_date DATETIME NOT NULL
        ###   j_name varchar(50) NOT NULL
        ###   notes MEDIUMTEXT NOT NULL
        addJuiceMods = ('''INSERT INTO juice_mods (mod_id, mod_date, j_name, notes) VALUES (%(emp_no)s, %(salary)s, %(from_date)s, %(to_date)s)''')

        # Insert new employee
        cursor.execute(addJuiceMods)

        # emp_no = cursor.lastrowid

        # Make sure data is committed to the database
        blendIt.commit()

        cursor.close()
        blendIt.close()


    def validEmail(self, email):
        try:
            v = validate_email(email) # validate and get info
            print v
            email = v["email"] # replace with normalized form
            return email
        except EmailNotValidError as evt:
            return str(evt)

